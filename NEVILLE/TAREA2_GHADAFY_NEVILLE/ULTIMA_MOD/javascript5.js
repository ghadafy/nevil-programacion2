//javascript 

//AQUI DEFINIMOS TODAS LAS CLASES Y FUNCIONES 

//CREAMOS LA CLASE VENDEDOR
class Vendedor{
	constructor (id, nombre, venta, comision){
		this.id = id
		this.nombre = nombre
		this.venta = venta
		this.comision = comision

	}

	muestraId(){
		return this.id
	}

	muestraNombre(){
		return this.nombre
	}

	muestraVenta(){
		return this.venta
	}

	muestraComision(){
		return this.comision
	}

	informacionCompleta(){
        return "Las ventas de "+this.nombre+" llegaron a B/. "+this.venta+", siendo esta la mayor, por lo que es merecedor de una comision de B/. "+this.comision
	}
	


}

//funciones que valida el nombre
const  ValidaNombre = (nombre,cont) => {
	cont2 = cont + 1
	while(nombre == "" || nombre==null){
        nombre = prompt("Informacion nula...Ingrese el nombre del Vendedor No."+cont2).toUpperCase()
	}
	return nombre
}

//funciones que valida el valor de la venta
const  ValidaVenta =(venta,nom) => {
	venta = parseFloat(venta)
	while(venta=="" || venta==null || isNaN(venta)){
		venta = prompt("Informacion no valida...Ingrese el monto de la venta de "+nom)
		venta = parseFloat(venta)
	}
	return venta
}

//funcion que determina la comision
 const com = (venta) => {
	if(venta>2500){
   		return 1000
	}
	else if(venta<=2499.99 &&  venta>=1000.00){
		return 300
	} 
	else{
		return 50
	}
}

/*
funcion que verifica cual es la mayor venta ingresada al arreglo
y la muestra en pantalla
*/
const MayorVenta = (arre,vend) => {
	let ind = 0, acumulador = 0, indice = 0
		while(ind<vend){
			if(arre[ind].venta>acumulador){
				acumulador=arre[ind].venta
				indice = ind 
				ind++
			}
			else{
				ind++
			}
		}
	console.log(indice)
    for(const nomb of arre){
		if(nomb.id==indice){
		alert(nomb.informacionCompleta())
	   }	
   }

}

//definimos las variables y pedimos los datos
let listaventas = []
let cont = 0, cont2 = 0 

let cantVend =  prompt("Ingrese la cantidad de vendedores a registrar: ") 

while(cont < parseInt(cantVend)){
    cont2 = cont + 1
	let vendedor = prompt("Ingrese el Nombre del vendedor No."+cont2).toUpperCase()
    vendedor = ValidaNombre(vendedor, cont)  
	
	let venta = prompt("Ingrese la venta de "+vendedor)
    venta = ValidaVenta(venta, vendedor)  

	listaventas.push(new Vendedor(cont, vendedor,venta, com(venta)))

	cont++
}

MayorVenta(listaventas,cantVend)
console.log(listaventas)

