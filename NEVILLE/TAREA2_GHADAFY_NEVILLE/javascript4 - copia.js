//javascript 
	
alert("Este script solicita la venta de tres personas y luego te muestra la venta mayor...")	
let continua = false

//Construimos el objeto
function Vendedor (nombre, venta, comision){

	this.nombre=nombre
	this.venta=venta
	this.comision=comision

} 

//funcion para calcular la comision
function com (comis){
	if(comis>2500){
   		return 1000
	}
	else if(comis<=2499.99 &&  comis>=1000.00){
		return 300
	} 
	else{
		return 50
	}
}


do{
//Pedimos la informacion	
let vendedores = 3
let cont = 1
let nombre = ""
let venta = 0 
let datos = []

while(cont <= vendedores){
 nombre = prompt("Ingrese el nombre del Vendedor No."+cont)
	//validamos el dato
 	while(nombre=="" || nombre==null){
        nombre = prompt("Informacion nula...Ingrese el nombre del Vendedor No."+cont)
	}

 venta = prompt("Ingrese el monto de la venta de "+nombre)
	//validamos el dato 
 	while(venta=="" || venta==null || isNaN(venta)){
		venta = prompt("Informacion no valida...Ingrese el monto de la venta de "+nombre)
	}	
	
//creamos el arreglo con los objetos
datos[cont-1] = new Vendedor(nombre, venta, com(venta))
cont++
}

//verificamos cual es la mayor venta cargada en el arreglo
let ind = 0, acumulador = 0, indice = 0
while(ind<vendedores){
	if(datos[ind].venta>acumulador){
		acumulador=datos[ind].venta
		indice = ind
		ind++
	}
	else{
		ind++
	}

}

alert("La mayor venta es de B/. "+datos[indice].venta+", y la realizo "+datos[indice].nombre+" ganandose una comision de B/. "+datos[indice].comision)

//Estos resultados los mostramos para verificar los registros captados
ind = 0
document.write("<table><tr><td>VENDEDOR</td><td>VENTA</td><td>COMISION</td></tr>")
while(ind<vendedores){
document.write("<tr><td>"+datos[ind].nombre+"</td><td>"+datos[ind].venta+"</td><td>"+datos[ind].comision+"</td></tr>")
ind++
}
document.write("</table>")

//continua = confirm("Desea ingresar nuevos valores")    
continua = false



}
while(continua)
