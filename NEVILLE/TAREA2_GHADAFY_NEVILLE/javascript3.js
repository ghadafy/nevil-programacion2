//javascript 
    
	//Inicializamos la variable que permite la nueva ejecución del programa cuando este termina  
	continua = false
	
	//Describimos lo que hace el programa 	
	alert("Este script registra la venta de tres Vendedores y luego te muestra la mejor de ellas y su correspondiente comision ...")
	
   do{
	
	//Declaramos las variables a utilizar
	const cant_vend = 3
	let nom1 = "", nom2 = "", nom3 = ""
	let ven1 = 0.0, ven2 = 0.0, ven3 = 0.0, i=0, j=0, cont=0, com = 0.00, venMay = 0.00, pos = 0
	let arregloNom = [], arregloVen = []
	
	//Pedimos el nombre de los vendedores y sus respectivas ventas
	while(i<cant_vend){
    cont = i+1		
	arregloNom[i] = prompt("Ingrese el nombre del vendedor No."+cont)
	
	    
		//Validamos el ingreso de datos
		while(arregloNom[i]=="" || arregloNom[i]==null){		
		arregloNom[i] = prompt("No se permiten datos nulos...Ingrese el nombre del vendedor No."+cont)
		}
			
		
		//Pedimos el monto de la venta
		arregloVen[j] = prompt("Ingrese el monto de la venta de "+arregloNom[i]);	
			
			//Validamos el monto de venta
			while(isNaN(arregloVen[j]) || arregloVen[j]=="" || arregloVen[j]==null){
			arregloVen[j] = prompt("Se espera un valor numerico...Ingrese el monto de la venta de "+arregloNom[i])
			}
		j++
	i++	
	}
	
    //Aqui buscamos la mayor venta
	for (j=0; j<cant_vend; j++){
		if(arregloVen[j]>=venMay){
			venMay=arregloVen[j]
			pos=j
		}		
	}
	
	//Determinamos la comision de la mayor venta
	if(venMay >= 2500.00){
	 com = 1000.00		
	}
	else if(venMay >= 1000.00 && venMay <= 2499.99){
	 com = 300.00	
	}
	else{
	 com = 50.00		
	}
	
	//Aqui mostramos los resultados
	alert("La Mayor venta la realizo: " +arregloNom[pos]+ " y fue de B/. " +venMay+ " y le corresponde una comision de: B/. " +com)
	
	//Datos para verificar
	document.write(arregloNom)
	document.write("<br>")
	document.write(arregloVen)
	

    //Aqui preguntamos si continuamos en el programa
    continua = confirm("Desea ingresar nuevos valores")
    
	}while(continua)
