//javascript 

//AQUI DEFINIMOS TODAS LAS FUNCIONES

//funcion para calcular la comision
function com (comis){
	if(comis>2500){
   		return 1000
	}
	else if(comis<=2499.99 &&  comis>=1000.00){
		return 300
	} 
	else{
		return 50
	}
}

//Construimos el objeto
function Vendedor (nombre, venta, comision){
	this.nombre=nombre
	this.venta=venta
	this.comision=comision
} 

//funciones que valida el nombre
function ValidaNombre(nombre,cont){
	while(nombre=="" || nombre==null){
        nombre = prompt("Informacion nula...Ingrese el nombre del Vendedor No."+cont)
	}
	return nombre
}

//funciones que valida  la venta
function ValidaVenta(venta,nom){
	venta = parseFloat(venta)
	while(venta=="" || venta==null || isNaN(venta)){
		venta = prompt("Informacion no valida...Ingrese el monto de la venta de "+nom)
		venta = parseFloat(venta)
	}
	return venta
}

//funcion que verifica cual es la mayor venta ingresada al arreglo
function MayorVenta(arre,vend){
let ind = 0, acumulador = 0, indice = 0
	while(ind<vend){
		if(arre[ind].venta>acumulador){
			acumulador=arre[ind].venta
			indice = ind 
			ind++
		}
		else{
			ind++
		}
	}
return indice
}
//AQUI TERMINA LA DECLARACION DE FUNCIONES

//AQUI INICIA LA CORRIDA DEL PROGRAMA
alert("Este script solicita la venta de tres personas y luego te muestra la venta mayor...")	
	
	//Inicializamos las variables a utilizar
	let vendedores = 3
	let cont = 1
	let datos = []

	while(cont <= vendedores){
		//Pedimos el nombre
		let	nombre = prompt("Ingrese el nombre del Vendedor No."+cont)
		
		//Llamamaos a la funcion para validar el dato
		nom = ValidaNombre(nombre,cont) 
		
		//Pedimos la venta
		let venta = prompt("Ingrese el monto de la venta de "+nom)
		
		//Llamamos a la funcion para validar el dato 
		ven = ValidaVenta(venta,nom)

		//Cargamos un arreglo con  objetos
		datos[cont-1] = new Vendedor(nom, ven, com(ven))
		
		cont++
	}
    //Llamamos a la funcion para 
	let res = MayorVenta(datos,vendedores)
	
	//Mostramos la mayor venta
	alert("La mayor venta es de B/. "+datos[res].venta+", y la realizo "+datos[res].nombre+" ganandose una comision de B/. "+datos[res].comision)

	//Mostramos el arreglo por consola para verificar
	console.log(datos)